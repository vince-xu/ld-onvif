package com.ld.onvif;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class PackageXml {
	/**----------------------------------- 功能 -------------------------------------**/
	/***
	 * 获取播放流地址
	 * @param username
	 * @param password
	 * @param token
	 * @return
	 */
	public static String streamUri(String username,String password,String token) {
		try {
			Map<String,String> account = WSUsenameToken(username,password);
			Resource resource = new ClassPathResource("wsdl/stream.wsdl");
			InputStream is = resource.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String data = null;
			while((data = br.readLine()) != null) {
			    sb.append(data);
			}
			br.close();
			isr.close();
			is.close();
			String streamXml = sb.toString().replace("{username}", account.get("username")).replace("{password}", account.get("password")).replace("{nonce}", account.get("nonce")).replace("{created}", account.get("created")).replace("{token}", token);
			return streamXml;
		} catch (IOException e) {
			return null;
		}
	}
	/***
	 * 预设位
	 * @param username
	 * @param password
	 * @param token
	 * @param preset 预设位
	 * @return
	 */
	public static String preset(String username,String password,String token,String preset) {
		try {
			Map<String,String> account = WSUsenameToken(username,password);
			Resource resource = new ClassPathResource("wsdl/preset.wsdl");
			InputStream is = resource.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String data = null;
			while((data = br.readLine()) != null) {
			    sb.append(data);
			}
			br.close();
			isr.close();
			is.close();
			String presetXml = sb.toString().replace("{username}", account.get("username")).replace("{password}", account.get("password")).replace("{nonce}", account.get("nonce")).replace("{created}", account.get("created")).replace("{token}", token).replace("{preset}", preset);
			return presetXml;
		} catch (IOException e) {
			return null;
		}
	}

	/***
	 * 云台控制
	 * @param username
	 * @param password
	 * @param token
	 * @param upDown 步长 -1~1 0.1~1 顺时针转动 -0.1~-1逆时针转动 0不转动
	 * @param leftRight 步长 -1~1 0.1~1 向上看 -0.1~-1 向下看 0不动
	 * @return
	 */
	public static String ptz(String username,String password,String token,Double leftRight,Double upDown,Double inOut) {
		try {
			Map<String,String> account = WSUsenameToken(username,password);
			Resource resource = new ClassPathResource("wsdl/ptz.wsdl");
			InputStream is = resource.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String data = null;
			while((data = br.readLine()) != null) {
				sb.append(data);
			}
			br.close();
			isr.close();
			is.close();
			String ptzXml = sb.toString().replace("{username}", account.get("username")).replace("{password}", account.get("password")).replace("{nonce}", account.get("nonce")).replace("{created}", account.get("created")).replace("{token}", token).replace("{leftRight}", leftRight.toString()).replace("{upDown}", upDown.toString()).replace("{inOut}", inOut.toString());
			return ptzXml;
		} catch (IOException e) {
			return null;
		}
	}



	/**----------------------------------- 鉴权 -------------------------------------**/
	
	public static String token(String username,String password) {
		try {
			Map<String,String> account = WSUsenameToken(username,password);
			Resource resource = new ClassPathResource("wsdl/token.wsdl");
			InputStream is = resource.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			StringBuffer sb = new StringBuffer();
			String data = null;
			while((data = br.readLine()) != null) {
			    sb.append(data);
			}
			br.close();
			isr.close();
			is.close();
			String tokenXml = sb.toString().replace("{username}", account.get("username")).replace("{password}", account.get("password")).replace("{nonce}", account.get("nonce")).replace("{created}", account.get("created"));
			return tokenXml;
		} catch (IOException e) {
			return null;
		}
	}
	
	public static Map<String,String> WSUsenameToken(String username,String password){
		try {
			//nonce
	        Random r = new Random();
	        String text = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	        String nonce = "";
	        for (int i = 0; i < 32; i++) {
	            int index = r.nextInt(text.length());
	            nonce = nonce + text.charAt(index);
	        }
			//time
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'",Locale.getDefault());
			String time = df.format(new Date());
			//password
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[] b1 = Base64.decodeBase64(nonce.getBytes());
			byte[] b2 = time.getBytes();
			byte[] b3 = password.getBytes();
			byte[] b4;
			md.update(b1, 0, b1.length);
	        md.update(b2, 0, b2.length);
	        md.update(b3, 0, b3.length);
	        b4 = md.digest();
	        String passwd = new String(Base64.encodeBase64(b4)).trim();
			//
			Map<String,String> result = new HashMap<String, String>();
			result.put("username", username);
			result.put("password", passwd);
			result.put("nonce", nonce);
			result.put("created", time);
			return result;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}
   
}
