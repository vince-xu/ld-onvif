package com.ld.onvif;

import com.ld.onvif.data.OnvifCodeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ld.onvif.data.OnvifResult;
@Slf4j
@Service
public class OnvifService {
	@Autowired
	SendOnvifCmd sendOnvifCmd;
	/**
	 *  获取rtsp流地址
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @return
	 */
	public OnvifResult live(String ip, int onvifPort , String onvifUsername, String onvifPassword) {
		try {
			String rtspUrl = sendOnvifCmd.stream(ip, onvifPort, onvifUsername, onvifPassword);
			if (rtspUrl != null) {
				return new OnvifResult(OnvifCodeEnum.OK, (Object) rtspUrl);
			} else {
				return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST);
			}
		}catch (Exception e){
			log.error("live 错误...");
			log.error(e.getMessage(),e);
			return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST,e.getMessage());
		}
	}

	/**
	 * preset 设置
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @param preset 预设位
	 * @return
	 */
	public OnvifResult preset(String ip, int onvifPort , String onvifUsername, String onvifPassword,String preset) {
		try {
			String result = sendOnvifCmd.preset(ip, onvifPort, onvifUsername, onvifPassword,preset);
			if (StringUtils.isEmpty(result)) {
				return new OnvifResult(OnvifCodeEnum.OK);
			} else {
				return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST,result);
			}
		}catch (Exception e){
			log.error("preset 错误...");
			log.error(e.getMessage(),e);
			return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST,e.getMessage());
		}
	}


	/**
	 *	ptz 控制
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @param command  1=上，2=下，3=左，4=右，5=左上，6=左下，7=右上，8=右下，0=停止，9=变倍小，10 = 变倍加
	 * @param speed 速度 -1 到 1 之间  默认 0.2
	 * @return
	 */
	public OnvifResult ptz(String ip, int onvifPort , String onvifUsername, String onvifPassword, String command , Double speed ){
		try{
			if(StringUtils .isEmpty(ip) || StringUtils .isEmpty(onvifUsername)  || StringUtils .isEmpty(onvifPassword)  || StringUtils .isEmpty(command)  ){
				return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST ,"参数不全");
			}
				if(speed == null){
					speed = 0.2;
				}
			if(speed > 1 && speed <-1) {
				return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST ,"speed 范围只能在 -1 和 1 之间");
			}
			Double leftRight = 0d,upDown = 0d,inOut = 0d;
			switch (command){
				case "0" :{
					leftRight = 0d;upDown = 0d;inOut = 0d;
					break;
				}
				case "1" :{
					leftRight = 0d;upDown = speed  ;inOut = 0d;
					break;
				}
				case "2" :{
					leftRight = 0d;upDown =  -speed;inOut = 0d;
					break;
				}
				case "3" :{
					leftRight = -speed ;upDown = 0d;inOut = 0d;
					break;
				}
				case "4" :{
					leftRight = speed ;upDown = 0d; inOut = 0d;
					break;
				}
				case "5" :{
					leftRight = -speed ;upDown = speed ;inOut = 0d;
					break;
				}
				case "6" :{
					leftRight = -speed ;upDown = -speed ;inOut = 0d;
					break;
				}
				case "7" :{
					leftRight = speed ;upDown = speed ;inOut = 0d;
					break;
				}
				case "8" :{
					leftRight = speed ;upDown =  -speed ;inOut = 0d;
					break;
				}
				case "9" :{
					leftRight = 0d ;upDown = 0d ;inOut =  -speed;
					break;
				}
				case "10" :{
					leftRight = 0d ;upDown = 0d ;inOut =  speed;
					break;
				}
			}
			String ptzResult = sendOnvifCmd.ptz(ip, onvifPort, onvifUsername, onvifPassword, leftRight, upDown, inOut);
			if(StringUtils.isEmpty(ptzResult)) {
				return new OnvifResult(OnvifCodeEnum.OK);
			} else {
				return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST,ptzResult);
			}
		}catch (Exception e){
			log.error("ptz 错误...");
			log.error(e.getMessage(),e);
			return new OnvifResult(OnvifCodeEnum.INVALID_REQUEST,e.getMessage());
		}
	}

}
