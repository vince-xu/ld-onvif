package com.ld.onvif.data;

import java.util.HashMap;


public class OnvifResult extends HashMap<String, Object>{
	public OnvifResult() {
	}
	public OnvifResult(OnvifCodeEnum codeEnum) {
		put("code", codeEnum.getCode());
		put("msg", codeEnum.getMsg());
	}
	public OnvifResult(OnvifCodeEnum codeEnum, Object data) {
		put("code", codeEnum.getCode());
		put("msg", codeEnum.getMsg());
		put("data", data);
	}
	public OnvifResult(OnvifCodeEnum codeEnum, String msg) {
		put("code", codeEnum.getCode());
		put("msg", msg);
	}
	public OnvifResult put(String key, Object value) {
		super.put(key, value);
		return this;
	}

}
