# ld-onvif

#### 介绍
参考  https://gitee.com/qinqi/JNVS  。onvif协议简单实现，提供给自己项目上面使用

#### 软件架构
spring boot 

#### 使用说明
OnvifService 提供了三个方法，live获取rtsp视频流，ptz云台控制，preset预置位控制

        /**
	 *  获取rtsp流地址
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @return
	 */
	public OnvifResult live(String ip, int onvifPort , String onvifUsername, String onvifPassword);


        /**
	 *
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @param preset 预设位
	 * @return
	 */
	public OnvifResult preset(String ip, int onvifPort , String onvifUsername, String onvifPassword,String preset) ;


        /**
	 *
	 * @param ip
	 * @param onvifPort
	 * @param onvifUsername
	 * @param onvifPassword
	 * @param command  1=上，2=下，3=左，4=右，5=左上，6=左下，7=右上，8=右下，0=停止，9=变倍小，10 = 变倍加
	 * @param speed 速度 -1 到 1 之间  默认 0.2
	 * @return
	 */
	public OnvifResult ptz(String ip, int onvifPort , String onvifUsername, String onvifPassword, String command , Double speed );



#### 特技
